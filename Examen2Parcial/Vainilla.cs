﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen2Parcial
{
    class Vainilla : AgregadoDecorator
    {
        //Se crea la clase Vainilla
        public Vainilla(JugoComponente bebida) : base(bebida) { }
        // Se agrega Costo
        public override double Costo => _bebida.Costo + 2;
        //Se agrega descripcion
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Vainilla");

    }
}
