﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen2Parcial
{
    public abstract class JugoComponente
    {
        // Se crea propiedad costo
        public abstract double Costo { get; }
        //Se crea propiedad Descripcion
        public abstract string Descripcion { get; }
    }
}

