﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen2Parcial
{
    // Se crea la clase Miel
    public class Miel : AgregadoDecorator
    {
        public Miel(JugoComponente bebida) : base(bebida) { }
        //Se agrega costo
        public override double Costo => _bebida.Costo + 1.50;
        // Se agrega descripcion
        public override string Descripcion => string.Format($"{_bebida.Descripcion}, Miel");

    }
}

